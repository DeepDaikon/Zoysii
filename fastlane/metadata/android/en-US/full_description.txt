Zoysii is a simple logic game. You are the red tile on a square board and the aim is to delete almost every tile while trying to make the most points.

It's <b>so easy</b>!

<b>Modes:</b>

‣ Single player: play a random match and try to get the most points.
‣ Multiplayer: play against your opponents and defeat them.
‣ Levels: use your mind to solve each level by deleting all the tiles.

<b>Main features:</b>

★ Multiplayer mode for up to 4 players on the same device
★ 70+ unique levels
★ 10+ numeral systems
★ Completely free
★ No Ads
★ Multiple languages
★ Minimalist design and dark mode

<b>Rules:</b>

The rules may seem difficult at first glance but they are not.

Anyhow, the best way to learn is by playing! Levels mode is a good place to start.

1. You are the red tile on a square board.

2. Swipe horizontally or vertically to move.

3. When you move you reduce tiles value in the direction you are going.

    - The amount of this reduction is equal to your starting point tile value.

    - But if the value of a tile would be equal to 1 or 2, there will be an increase instead of a decrease.

    - Negative numbers become positive.

    - If the value of a tile becomes equal to zero, starting tile value becomes zero too. Tiles have been "Deleted".

4. You earn as many points as the value of the deleted tiles.

5. The aim is to delete almost every tile while trying to make the most points.

6. In multiplayer matches a player can win by deleting opponent's tile.
