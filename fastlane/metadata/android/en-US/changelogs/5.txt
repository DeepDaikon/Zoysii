* Add numeral system option
* Add range of values option
* Add rule 3b option
* Update levels
* Improve readability on small screens
* Minor UI improvements
* Bug fixes
