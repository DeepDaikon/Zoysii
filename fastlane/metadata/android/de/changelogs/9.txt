* 7 neue Ebenen
* Neues UI (Startseite, Regeln, Ebenen)
* Generelle UI/UX Verbesserungen
* Unterstützung von Android 10 Dunkles Thema
* Fehlerbehebungen
