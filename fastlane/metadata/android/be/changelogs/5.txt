* Дададзена опцыя змены сістэм злічэння
* Дададзена опцыя змены дыяпазону значэнняў
* Дададзена опцыя правілы 3b
* Абнаўленне узроўняў
* Паляпшэнне чытальнасці на маленькіх экранах
* Нязначныя паляпшэнні UI
* Выпраўленне памылак
