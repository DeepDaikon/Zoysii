Zoysiiはシンプルなロジックゲームです。正方形のボード上の赤いタイルがあなたです、ポイントをできるだけ増やしつつほとんどのタイルを消していましょう。

It's <b>so easy</b>!

<b>モード</b>

‣ シングルプレイヤー : play a random match and try to get the most points.
‣ マルチプレイヤー : play against your opponents and defeat them.
‣ レベル: use your mind to solve each level by deleting all the tiles.

<b>Main features:</b>

★ Multiplayer mode for up to 4 players on the same device
★ 70+ unique levels
★ 10+ numeral systems
★ Completely free
★ No Ads
★ Multiple languages
★ Minimalist design and dark mode

<b>ルール</b>

ルールは一見難しそうですが、そうでもありません。

とにかく、プレイして覚えるのが一番です。まずはレベルモードから始めるのがいいでしょう。

1. 正方形のボード上の赤いタイルがあなたです。

2. 横または縦にスワイプして移動します。

3. 移動すると、向かった方向のタイル値が減少します。

    - この減少量は、出発地点のタイルの値と同じです。

    - ただしタイルの値が1または2になるときは、減少ではなく増加します。

    - マイナスの数字はプラスに変わります。

    - タイルの値が0になると、出発地点のタイルの値も0になります。タイルは消えます。

4. 消えたタイルと同じ値をポイントとして獲得します。

5. ポイントをできるだけ増やしつつ、ほとんどのタイルを消していくことが目的です。

6. マルチプレイヤー戦では、対戦相手のタイルを消すことによっても勝利できます.
