Zoysii é um jogo de lógica simples. Você é a peça vermelha em um tabuleiro quadrado e o objetivo é deletar quase todas as peças enquanto tenta fazer o máximo de pontos.

Isto é <b>muito fácil</b>!

<b>Modos:</b>

‣ Único jogador: jogue uma partida aleatória e tente conseguir o máximo de pontos.
‣ Multiplayer: jogue contra seus oponentes e derrote-os.
‣ Níveis: use sua mente para resolver cada nível, excluindo todas as peças.

<b>Principais características:</b>

★ Modo multiplayer para até 4 jogadores no mesmo dispositivo
★ Mais de 70 níveis únicos
★ 10+ sistemas numéricos
★ Completamente grátis
★ Sem anúncios
★ Vários idiomas
★ Design minimalista e modo escuro

<b>Regras:</b>

As regras podem parecer difíceis à primeira vista, mas não são.

De qualquer forma, a melhor forma de aprender é brincando! O modo de níveis é um bom lugar para começar.

1. Você é a peça vermelha em um tabuleiro quadrado.

2. Deslize horizontalmente ou verticalmente para mover.

3. Ao se mover, você reduz o valor das peças na direção em que está indo.

    - O valor dessa redução é igual ao valor da peça do seu ponto inicial.

    - Mas se o valor de uma peça for igual a 1 ou 2, haverá um aumento em vez de uma diminuição.

    - Os números negativos tornam-se positivos.

    - Se o valor de um bloco se tornar igual a zero, o valor inicial do bloco também se tornará zero. Os blocos foram "excluídos".

4. Você ganha tantos pontos quanto o valor das peças excluídas.

5. O objetivo é deletar quase todas as peças enquanto tenta fazer o máximo de pontos.

6. Em partidas multiplayer, um jogador pode vencer excluindo a peça do oponente.
