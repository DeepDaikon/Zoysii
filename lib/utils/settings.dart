import 'dart:io';

import 'package:flutter/material.dart';
import 'package:zoysii/utils/i18n.dart';
import 'package:zoysii/utils/local_data_controller.dart';
import 'package:zoysii/utils/numeral_system.dart';

/// App settings
class Settings {
  Settings(Map<String, dynamic> json)
      : sideLength = json['sideLength'] ?? 6,
        inputMethod = json['inputMethod'] ?? 0,
        gamepadSize = json['gamepadSize'] ?? 50,
        gamepadShape = json['gamepadShape'] ?? 0,
        gamepadOffset =
            Offset(json['gamepadOffsetX'] ?? 0, json['gamepadOffsetY'] ?? 0),
        minBoardInt = json['minBoardInt'] ?? 1,
        maxBoardInt = json['maxBoardInt'] ?? 19,
        biggestLow = json['biggestLow'] ?? 2,
        _numeralSystem = json['numeralSystem'] ?? 0,
        players = (json['players'] ?? [true, true, false]).cast<bool>(),
        speedMulti = json['speedMulti'] ?? 0,
        rotatedDisplay = json['rotatedDisplay'] ?? false,
        firstRun = json['firstRun'] ?? results.isEmpty,
        bottomBar = json['bottomBar'] ?? true,
        theme = ThemeMode
            .values[json['theme'] ?? ((json['darkTheme'] ?? false) ? 2 : 0)],
        _languageCode = json['languageCode'];

  Map<String, dynamic> toJson() => {
        'sideLength': sideLength,
        'inputMethod': inputMethod,
        'gamepadSize': gamepadSize,
        'gamepadShape': gamepadShape,
        'gamepadOffsetX': gamepadOffset.dx,
        'gamepadOffsetY': gamepadOffset.dy,
        'minBoardInt': minBoardInt,
        'maxBoardInt': maxBoardInt,
        'biggestLow': biggestLow,
        'numeralSystem': _numeralSystem,
        'theme': theme.index,
        'bottomBar': bottomBar,
        'players': players,
        'speedMulti': speedMulti,
        'rotatedDisplay': rotatedDisplay,
        'firstRun': firstRun,
        'languageCode': _languageCode,
      };

  /// Grid side length
  int sideLength;

  /// Selected input method
  int inputMethod;

  /// Virtual gamepad dimension, shape and position
  double gamepadSize;
  int gamepadShape;
  late Offset gamepadOffset;

  /// Board range values
  int maxBoardInt;
  int minBoardInt;

  /// Biggest "low number" for rule 3b
  int biggestLow;

  /// Numeral notation
  int _numeralSystem;
  NumeralSystem get numeralSystem => NumeralSystem.values[_numeralSystem];
  set numeralSystem(NumeralSystem value) => _numeralSystem = value.index;

  /// App theme
  ThemeMode theme;

  /// If true show GameAppBar on bottom
  bool bottomBar;

  /// Multiplayer preferences
  /// [players]: list of bool (human or not)
  List<bool> players;
  int speedMulti;
  bool rotatedDisplay;

  /// True if it is the first run of the app
  bool firstRun;

  /// App language
  String? _languageCode;
  bool get useSystemLanguage => _languageCode == null;
  String get _currentLanguageCode =>
      useSystemLanguage ? Platform.localeName : _languageCode!;
  Locale get locale => supportedLocales
              .contains(Locale(_currentLanguageCode.split('_').first)) ||
          !useSystemLanguage
      ? Locale(_currentLanguageCode.split('_').first)
      : const Locale('en');
  set locale(Locale? locale) => _languageCode = locale?.toString();
}
