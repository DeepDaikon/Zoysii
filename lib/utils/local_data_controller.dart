import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:zoysii/game/utils/level.dart';
import 'package:zoysii/game/utils/result.dart';
import 'package:zoysii/utils/settings.dart';

/// Game settings and preferences
late Settings settings;
late SharedPreferences _prefs;

/// List of match results
List<Result> results = [];

/// Next level to play
/// nextLevel[0] == layer, nextLevel[1] == level
List<int> nextLevel = [0, 0];

/// Game levels and layers (groups of levels)
List<String> layers = [];
List<Level> levels = [];

/// Import all data from shared preferences
Future<void> loadStoredData() async {
  _prefs = await SharedPreferences.getInstance();
  settings = Settings(jsonDecode(_prefs.getString('settings') ?? '{}'));

  // Restore results from shared preferences
  var jsonResults = jsonDecode(_prefs.getString('results_v3') ?? '[]');
  for (final jResult in jsonResults) {
    results.add(Result.fromJson(jResult));
  }

  // Restore the next level to play
  nextLevel[0] = _prefs.getInt('lastWorldSolved') ?? 0;
  nextLevel[1] = _prefs.getInt('lastLevelSolved') ?? 0;

  // Load levels from assets
  var data = json.decode(await rootBundle.loadString('assets/levels.json'));
  layers.addAll(data['layers'].cast<String>());
  data['levels'].forEach((level) => levels.add(Level(level)));
}

/// Save a new result in shared preferences
void saveResult(Result result) {
  results.add(result);
  _prefs.setString('results_v3',
      jsonEncode(results.fold([], (l, r) => (l as List)..add(r.toJson()))));
}

/// Save app settings in shared preferences
void saveSettings() {
  _prefs.setString('settings', jsonEncode(settings.toJson()));
}

/// Save next level to play in shared preferences
void saveNextLevel(List<int> levelWon) {
  if (listEquals(levelWon, nextLevel)) {
    ++nextLevel[1];
    if (nextLevel[1] > 6) {
      ++nextLevel[0];
      nextLevel[1] = 0;
    }
    _prefs.setInt('lastWorldSolved', nextLevel[0]);
    _prefs.setInt('lastLevelSolved', nextLevel[1]);
  }
}
