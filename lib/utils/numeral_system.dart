/// List of possible number representations
// i18n: 'Arabic'.i18n, 'Hex'.i18n, 'Letters'.i18n, 'Aegean'.i18n,
// 'Braille'.i18n, 'Khmer'.i18n, 'Armenian'.i18n, 'Burmese'.i18n, 'Farsi'.i18n,
// 'Chinese'.i18n, 'Roman'.i18n
// ignore_for_file: constant_identifier_names
enum NumeralSystem {
  Arabic,
  Hex,
  Letters,
  Aegean('NotoSansLinearB'),
  Braille('NotoSansBraille'),
  Khmer('NotoSansKhmer'),
  Armenian('NotoSansArmenian'),
  Burmese('NotoSansMyanmar'),
  Farsi,
  Chinese,
  Roman;

  const NumeralSystem([this.fontFamily]);
  final String? fontFamily;
}

/// Extension used to handle conversion to supported number notations
extension Notations on int? {
  /// Convert input number to a string representation in the given notation
  String toNotation(NumeralSystem notation) {
    if (this == 0) return notation != NumeralSystem.Braille ? '•' : '⠚';
    if (this == null) return ' ';
    var sign = '';
    var value = this;
    if (this! < 0) {
      sign = '-';
      value = value!.abs();
    }
    switch (notation) {
      case NumeralSystem.Arabic:
        return sign + value.toString();
      case NumeralSystem.Hex:
        return sign + value!.toRadixString(16).toUpperCase();
      case NumeralSystem.Letters:
        return sign + value._toLetter();
      case NumeralSystem.Aegean:
        return sign + value._toAegean();
      case NumeralSystem.Armenian:
        return sign + value._toArmenian();
      case NumeralSystem.Khmer:
        return sign + value._toKhmer();
      case NumeralSystem.Braille:
        return sign + value._toBraille();
      case NumeralSystem.Burmese:
        return sign + value._toBurmese();
      case NumeralSystem.Farsi:
        return sign + value._toFarsi();
      case NumeralSystem.Chinese:
        return sign + value._toChinese();
      case NumeralSystem.Roman:
        return sign + value._toRoman();
    }
  }

  /// Convert int to its letter representation
  /// A == 1, B == 2, Z == 26, AA == 27, AB == 28, ...
  String _toLetter() {
    if (this! > 702) return toString();
    var alphabet = ' ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    var intDivide = this! ~/ 26;
    var remainder = this! % 26;
    return (alphabet[intDivide - (remainder != 0 ? 0 : 1)] +
            alphabet[remainder != 0 ? remainder : 26])
        .trim();
  }

  /// Convert this into Khmer number
  String _toKhmer() => _replaceWith('០១២៣៤៥៦៧៨៩');

  /// Convert this into Braille number
  String _toBraille() => _replaceWith('⠚⠁⠃⠉⠙⠑⠋⠛⠓⠊');

  /// Convert this into Farsi number
  String _toFarsi() => _replaceWith('۰۱۲۳۴۵۶۷۸۹');

  /// Convert this into Burmese number
  String _toBurmese() => _replaceWith('၀၁၂၃၄၅၆၇၈၉');

  /// Convert this into Roman number
  String _toRoman() => _replaceWithUTH(
          U: ['', 'I', 'II', 'III', 'IV', 'V', 'VI', 'VII', '\nVIII', 'IX'],
          T: ['', 'X', 'XX', 'XXX\n', 'XL', 'L', 'LX', 'LXX\n', 'LXXX\n', 'XC'],
          H: ['', 'C', 'CC', 'CCC\n', 'CD', 'D', 'DC', 'DCC\n', 'DCCC\n', 'CM'])
      .trim();

  /// Convert this into Aegean number
  String _toAegean() => _replaceWithUTH(
      U: ['', '𐄇', '𐄈', '𐄉', '𐄊', '𐄋', '𐄌', '𐄍', '𐄎', '𐄏'],
      T: ['', '𐄐', '𐄑', '𐄒', '𐄓', '𐄔', '𐄕', '𐄖', '𐄗', '𐄘'],
      H: ['', '𐄙', '𐄚', '𐄛', '𐄜', '𐄝', '𐄞', '𐄟', '𐄠', '𐄡']);

  /// Convert this into Armenian number
  String _toArmenian() => _replaceWithUTH(
      U: ['', 'Ա', 'Բ', 'Գ', 'Դ', 'Ե', 'Զ', 'Է', 'Ը', 'Թ'],
      T: ['', 'Ժ', 'Ի', 'Լ', 'Խ', 'Ծ', 'Կ', 'Հ', 'Ձ', 'Ղ'],
      H: ['', 'Ճ', 'Մ', 'Յ', 'Ն', 'Շ', 'Ո', 'Չ', 'Պ', 'Ջ']);

  /// Convert this into Chinese number
  String _toChinese() {
    var thisAsString = toString();
    var output = '';
    var I = ['', '一', '二', '三', '四', '五', '六', '七', '八', '九'];
    var X = ['', '十', '百'];
    for (var i = 0; i < thisAsString.length; i++) {
      output += (thisAsString[i] == '1' && i == 0 && thisAsString.length == 2
              ? ''
              : I[int.parse(thisAsString[i])]) +
          (int.parse(thisAsString[i]) != 0
              ? X[thisAsString.length - 1 - i]
              : '');
    }
    return output;
  }

  /// Replace digits with input notation
  String _replaceWith(String notation) =>
      toString().split('').fold('', (prev, e) => prev + notation[int.parse(e)]);

  /// Replace digits with input notation for units (U), tens (T) and hundreds (H)
  String _replaceWithUTH(
          {required List<String> U,
          required List<String> T,
          required List<String> H}) =>
      this! > 999
          ? toString()
          : H[((this! % 1000) ~/ 100)] +
              T[((this! % 100) ~/ 10)] +
              U[this! % 10];
}
