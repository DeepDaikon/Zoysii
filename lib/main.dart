import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:i18n_extension/i18n_widget.dart';
import 'package:window_manager/window_manager.dart';
import 'package:zoysii/ui/basic.dart';
import 'package:zoysii/ui/screens/home/home_page.dart';
import 'package:zoysii/ui/screens/rules/rules_page.dart';
import 'package:zoysii/ui/themes.dart';
import 'package:zoysii/utils/i18n.dart';
import 'package:zoysii/utils/local_data_controller.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await loadStoredData();
  await SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
  await Localization.loadTranslations();
  if (Platform.isWindows || Platform.isLinux || Platform.isMacOS) {
    await windowManager.ensureInitialized();
    windowManager.setMinimumSize(const Size(400, 600));
    windowManager.setMaximumSize(Size.infinite);
  }
  runApp(Zoysii());
}

class Zoysii extends StatefulWidget {
  static State<Zoysii> of(BuildContext context) =>
      context.findAncestorStateOfType()!;

  @override
  State<Zoysii> createState() => _ZoysiiState();
}

class _ZoysiiState extends State<Zoysii> {
  @override
  Widget build(BuildContext context) {
    return I18n(
      initialLocale: settings.locale,
      child: MaterialApp(
        title: 'Zoysii',
        theme: lightTheme,
        darkTheme: darkTheme,
        themeMode: settings.theme,
        home: settings.firstRun ? RulesPage(HomePage()) : HomePage(),
        scrollBehavior: AppScrollBehavior(),
        localizationsDelegates: const [
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        supportedLocales: supportedLocales,
        debugShowCheckedModeBanner: false,
      ),
    );
  }
}
