import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:zoysii/game/game.dart';
import 'package:zoysii/game/utils/player.dart';
import 'package:zoysii/ui/screens/game/game_page.dart';
import 'package:zoysii/utils/i18n.dart';

/// Colors used for each player
const List<Color> playerColor = [
  Colors.redAccent,
  Colors.blueAccent,
  Colors.orange,
  Colors.green,
];

extension ColorName on Color {
  String get name {
    if (value == Colors.redAccent.value) return 'Red'.i18n;
    if (value == Colors.blueAccent.value) return 'Blue'.i18n;
    if (value == Colors.orange.value) return 'Yellow'.i18n;
    if (value == Colors.green.value) return 'Green'.i18n;
    return '-';
  }
}

/// Linear gradient used in app
const appGradient = LinearGradient(colors: [
  Color(0xFF103b4f),
  Color(0xFF1c3b4f),
  Color(0xFF293b4f),
  Color(0xFF433b4f),
  Color(0xFF5c3b4f),
  Color(0xFF693b4f),
  Color(0xFF763b4f),
], begin: Alignment.topLeft, end: Alignment.bottomRight);

const darkAppGradient = LinearGradient(colors: [
  Color(0xFF393939),
  Color(0xFF323232),
  Color(0xFF232323),
  Color(0xFF000000),
], begin: Alignment.topLeft, end: Alignment.bottomRight);

/// Shape used for menu buttons
const menuButtonShape =
    RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(20)));

/// BorderRadius used for dialog and cards
const borderRadius = BorderRadius.only(
    topRight: Radius.circular(20),
    topLeft: Radius.circular(5),
    bottomLeft: Radius.circular(20),
    bottomRight: Radius.circular(5));

/// Page route
class FadeRoute extends PageRouteBuilder {
  FadeRoute(this.page)
      : super(
            pageBuilder: (
              BuildContext context,
              Animation<double> animation,
              Animation<double> secondaryAnimation,
            ) =>
                page,
            transitionsBuilder: (
              BuildContext context,
              Animation<double> animation,
              Animation<double> secondaryAnimation,
              Widget child,
            ) =>
                FadeTransition(opacity: animation, child: child));

  @override
  final Duration transitionDuration = const Duration(milliseconds: 150);
  final Widget page;
}

class AppScrollBehavior extends MaterialScrollBehavior {
  @override
  Set<PointerDeviceKind> get dragDevices => {
        PointerDeviceKind.touch,
        PointerDeviceKind.mouse,
      };
}

/// Floating "PLAY" button
class PlayButton extends StatelessWidget {
  PlayButton({
    this.players,
    this.gameSpeed,
    this.callback,
  });
  final List<Player>? players;
  final int? gameSpeed;
  final VoidCallback? callback;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 150,
      height: 50,
      decoration: BoxDecoration(
          color: Theme.of(context).brightness == Brightness.dark
              ? const Color(0xFF2C2E30)
              : Theme.of(context).primaryColor,
          borderRadius: borderRadius),
      child: TextButton(
        style: ButtonStyle(
            shape: MaterialStateProperty.all(
                const RoundedRectangleBorder(borderRadius: borderRadius))),
        onPressed: players?.every((p) => !p.human) ?? false
            ? null
            : () {
                Navigator.push(
                    context,
                    FadeRoute(
                      GamePage(
                        (players?.length ?? 1) == 1
                            ? GameMode.single
                            : GameMode.multi,
                        players: players,
                        gameSpeed: gameSpeed,
                      ),
                    )).then((_) => callback?.call());
              },
        child: FittedBox(
          fit: BoxFit.fitWidth,
          child: Text(
            'Play'.i18n.toUpperCase(),
            maxLines: 1,
            style: const TextStyle(
                color: Colors.white, fontSize: 20, letterSpacing: 8),
          ),
        ),
      ),
    );
  }
}
