import 'package:flutter/material.dart';

final ThemeData lightTheme = ThemeData(
  colorSchemeSeed: const Color(0xFF244A63),
  dividerColor: Colors.grey.shade300,
  appBarTheme: const AppBarTheme(
      titleTextStyle: TextStyle(
          letterSpacing: 6, fontSize: 22, fontWeight: FontWeight.w600)),
  toggleableActiveColor: const Color(0xFF006491),
);

final ThemeData darkTheme = ThemeData(
  brightness: Brightness.dark,
  colorSchemeSeed: const Color(0xFF244A63),
  dividerColor: Colors.grey.shade300,
  appBarTheme: const AppBarTheme(
      titleTextStyle: TextStyle(
          letterSpacing: 6, fontSize: 22, fontWeight: FontWeight.w600)),
  toggleableActiveColor: const Color(0xFF006491),
);
