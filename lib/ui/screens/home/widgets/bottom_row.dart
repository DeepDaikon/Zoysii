import 'package:flutter/material.dart';
import 'package:zoysii/ui/basic.dart';
import 'package:zoysii/ui/screens/info/info_page.dart';
import 'package:zoysii/ui/screens/ranking/ranking_page.dart';
import 'package:zoysii/ui/screens/rules/rules_page.dart';
import 'package:zoysii/ui/screens/settings/settings_page.dart';
import 'package:zoysii/utils/i18n.dart';

class BottomRow extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        _button(context, Icons.equalizer, 'Ranking'.i18n, RankingPage()),
        _button(context, Icons.help_outline, 'Rules'.i18n, RulesPage()),
        Expanded(
          child: Container(
            margin: const EdgeInsets.symmetric(horizontal: 40),
            height: 2,
            color: Colors.white.withOpacity(0.3),
          ),
        ),
        _button(context, Icons.info_outline, 'Info'.i18n, InfoPage()),
        _button(context, Icons.settings, 'Settings'.i18n, SettingsPage()),
      ],
    );
  }

  Widget _button(BuildContext context, IconData icon, String tooltip,
      StatefulWidget page) {
    return IconButton(
      icon: Icon(icon, color: Colors.white.withOpacity(0.7), size: 28),
      tooltip: tooltip,
      onPressed: () => Navigator.push(context, FadeRoute(page)),
    );
  }
}
