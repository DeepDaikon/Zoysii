import 'package:flutter/material.dart';
import 'package:zoysii/ui/basic.dart';

class PageButton extends StatefulWidget {
  PageButton({required this.title, required this.onPressed});
  final String title;
  final VoidCallback onPressed;

  @override
  State<PageButton> createState() => _PageButtonState();
}

class _PageButtonState extends State<PageButton> {
  bool focused = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: const BoxConstraints(maxWidth: 300),
      width: MediaQuery.of(context).size.width / 1.5,
      height: 50,
      margin: const EdgeInsets.only(bottom: 25),
      decoration: BoxDecoration(
          color: focused ? Colors.grey.shade300 : Colors.white,
          border: Border.all(color: Colors.black45),
          borderRadius: borderRadius),
      child: TextButton(
        onFocusChange: (f) => setState(() => focused = f),
        style: ButtonStyle(
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                const RoundedRectangleBorder(borderRadius: borderRadius))),
        onPressed: widget.onPressed,
        child: FittedBox(
          fit: BoxFit.fitWidth,
          child: Text(widget.title,
              style: const TextStyle(color: Colors.black, fontSize: 21)),
        ),
      ),
    );
  }
}
