import 'package:flutter/material.dart';
import 'package:zoysii/game/game.dart';
import 'package:zoysii/ui/basic.dart';
import 'package:zoysii/ui/screens/game/game_page.dart';
import 'package:zoysii/ui/screens/home/widgets/bottom_row.dart';
import 'package:zoysii/ui/screens/home/widgets/page_button.dart';
import 'package:zoysii/ui/screens/levels/levels_page.dart';
import 'package:zoysii/ui/screens/multiplayer/multiplayer_page.dart';
import 'package:zoysii/utils/i18n.dart';

class HomePage extends StatelessWidget {
  List<Map<String, dynamic>> pageList() => [
        {'title': 'Quick match'.i18n, 'goto': () => GamePage(GameMode.single)},
        {'title': 'Multiplayer'.i18n, 'goto': MultiPlayerPage.new},
        {'title': 'Levels'.i18n, 'goto': LevelsPage.new},
      ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: DecoratedBox(
        decoration: BoxDecoration(
            gradient: Theme.of(context).brightness == Brightness.light
                ? appGradient
                : darkAppGradient),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            const Expanded(
              flex: 5,
              child: Center(
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 30),
                  child: FittedBox(
                    fit: BoxFit.fitWidth,
                    child: Text(
                      'ZOYSII',
                      style: TextStyle(
                        letterSpacing: 8,
                        fontSize: 80,
                        color: Colors.white,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                ),
              ),
            ),
            for (var page in pageList())
              PageButton(
                title: page['title'],
                onPressed: () {
                  Navigator.push(context, FadeRoute(page['goto']()));
                },
              ),
            const Spacer(),
            BottomRow(),
          ],
        ),
      ),
    );
  }
}
