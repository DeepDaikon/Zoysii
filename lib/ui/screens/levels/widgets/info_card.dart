import 'package:flutter/material.dart';
import 'package:zoysii/ui/screens/levels/widgets/card_button.dart';
import 'package:zoysii/utils/i18n.dart';

class InfoCard extends StatelessWidget {
  InfoCard({required this.firstCard});
  final bool firstCard;

  @override
  Widget build(BuildContext context) {
    String title;
    String subtitle;
    var buttons = <Widget>[];
    if (firstCard) {
      title = 'Welcome!'.i18n;
      subtitle = '\n\n' +
          'In this mode you have to solve each level by deleting all the tiles.\n\nLevels are grouped in "Layers": 7 in each of them. You have to solve a level to unlock the next one.\n\nSwipe to right and select the first level.\n\nGood luck!'
              .i18n;
    } else {
      title = 'Congratulations!'.i18n;
      subtitle = '\n' +
          'You have completed all the available Layers!\n\nThe next one will be released soon.\n\nIn the meantime you can support the development of Zoysii.'
              .i18n;
      buttons.addAll([
        CardButton('Donate'.i18n, 'https://deepdaikon.xyz/donate'),
        CardButton('Translate'.i18n,
            'https://translate.deepdaikon.xyz/engage/zoysii/'),
        CardButton(
            'Report bugs'.i18n, 'https://gitlab.com/deepdaikon/Zoysii/issues'),
      ]);
    }
    return Container(
      margin: const EdgeInsets.all(20),
      child: Column(children: [
        Center(
          child: FittedBox(
            fit: BoxFit.fitWidth,
            child: Text(title, style: const TextStyle(fontSize: 40)),
          ),
        ),
        Text(subtitle, style: const TextStyle(fontSize: 20)),
        ...buttons,
      ]),
    );
  }
}
