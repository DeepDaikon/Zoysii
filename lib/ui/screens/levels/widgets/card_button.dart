import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:zoysii/ui/basic.dart';

class CardButton extends StatelessWidget {
  CardButton(this.text, this.url);
  final String text;
  final String url;

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: const BoxConstraints(maxWidth: 300),
      width: MediaQuery.of(context).size.width / 1.5,
      height: 50,
      margin: const EdgeInsets.only(top: 20),
      decoration: BoxDecoration(
          border: Border.all(color: Theme.of(context).colorScheme.secondary),
          borderRadius: borderRadius),
      child: TextButton(
          style: ButtonStyle(
              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  const RoundedRectangleBorder(borderRadius: borderRadius))),
          child: Text(text, style: const TextStyle(fontSize: 21)),
          onPressed: () =>
              launchUrl(Uri.parse(url), mode: LaunchMode.externalApplication)),
    );
  }
}
