import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:zoysii/ui/basic.dart';
import 'package:zoysii/ui/screens/levels/widgets/info_card.dart';
import 'package:zoysii/ui/screens/levels/widgets/layer_card.dart';
import 'package:zoysii/utils/i18n.dart';
import 'package:zoysii/utils/local_data_controller.dart';

/// Selected layer
int pageIndex = listEquals(nextLevel, [0, 0]) ? 0 : nextLevel[0] + 1;
late PageController pageController;

class LevelsPage extends StatefulWidget {
  @override
  State<LevelsPage> createState() => _LevelsPageState();
}

class _LevelsPageState extends State<LevelsPage> {
  @override
  void initState() {
    super.initState();
    pageController =
        PageController(initialPage: pageIndex, viewportFraction: 0.9);
  }

  @override
  Widget build(BuildContext context) {
    var pages = nextLevel[0] + (nextLevel[0] >= 1 ? 1 : 2);
    return DecoratedBox(
      decoration: Theme.of(context).brightness == Brightness.light
          ? const BoxDecoration(gradient: appGradient)
          : BoxDecoration(color: Colors.grey[900]),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.transparent,
          leading: IconButton(
            icon: const Icon(Icons.close, color: Colors.white, size: 32),
            onPressed: Navigator.of(context).pop,
            tooltip: 'Close'.i18n,
          ),
        ),
        body: PageView.builder(
          itemCount: pages,
          controller: pageController,
          onPageChanged: (int newIndex) => setState(() => pageIndex = newIndex),
          itemBuilder: (_, i) {
            return Center(
              child: SingleChildScrollView(
                primary: false,
                child: Container(
                  margin: const EdgeInsets.symmetric(horizontal: 12),
                  constraints: const BoxConstraints(maxWidth: 600),
                  decoration: BoxDecoration(
                      color: Theme.of(context).cardColor,
                      borderRadius: borderRadius),
                  child: i >= layers.length || (i == 0 && nextLevel[0] == 0)
                      ? InfoCard(firstCard: i == 0)
                      : LayerCard(i - (nextLevel[0] >= 1 ? 0 : 1),
                          () => setState(() {})),
                ),
              ),
            );
          },
        ),
        bottomNavigationBar: Container(
          height: 48,
          padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              for (var i = 0; i < pages; i++)
                CircleAvatar(
                  radius: pageIndex == i ? 4 : 2,
                  backgroundColor:
                      pageIndex == i ? (Colors.white) : Colors.grey,
                ),
            ],
          ),
        ),
      ),
    );
  }
}
