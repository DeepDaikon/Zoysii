import 'package:flutter/material.dart';
import 'package:zoysii/game/game.dart';
import 'package:zoysii/utils/numeral_system.dart';

class GameGrid extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GridView.count(
      padding: const EdgeInsets.symmetric(vertical: 10),
      physics: const NeverScrollableScrollPhysics(),
      childAspectRatio:
          (MediaQuery.of(context).size.width / game!.board!.width) /
              ((MediaQuery.of(context).size.height -
                      MediaQuery.of(context).padding.vertical -
                      115) /
                  game!.board!.height),
      crossAxisCount: game!.board!.width,
      shrinkWrap: true,
      children: _gridComposer(context),
    );
  }

  /// Generate game grid and return list of tile widgets
  List<Widget> _gridComposer(BuildContext context) {
    // Map<position, color_value>
    // It only contains players' position as keys
    var colorMap = <int, int>{};
    var bottomBorder = <int, int>{};
    game!.players.asMap().forEach((index, player) {
      if (player.alive) {
        colorMap[player.position] =
            (colorMap[player.position] ?? 0) + player.displayColor.value;
        if (game!.canMove(player)) {
          bottomBorder[player.position] =
              (bottomBorder[player.position] ?? 0) + player.displayColor.value;
        }
      }
    });

    return List.generate(game!.board!.grid.length, (index) {
      return Center(
        child: Container(
          margin: const EdgeInsets.symmetric(horizontal: 2),
          decoration: bottomBorder[index] == null
              ? null
              : game!.selectedNumeralSystem == NumeralSystem.Chinese
                  ? BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: Color(bottomBorder[index]!).withOpacity(0.2),
                          spreadRadius: 20,
                          blurRadius: 50,
                        ),
                      ],
                    )
                  : BoxDecoration(
                      border: Border(
                        bottom: BorderSide(
                          color: Color(bottomBorder[index]!),
                          width: 3,
                        ),
                      ),
                    ),
          child: FittedBox(
            child: Text(
              game!.board!.grid[index].toNotation(game!.selectedNumeralSystem),
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 28,
                color: Color(colorMap[index] ??
                    Theme.of(context).colorScheme.onBackground.value),
                fontFamily: game!.selectedNumeralSystem.fontFamily,
              ),
            ),
          ),
        ),
      );
    });
  }
}
