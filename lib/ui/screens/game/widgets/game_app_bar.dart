import 'package:flutter/material.dart';
import 'package:zoysii/game/game.dart';
import 'package:zoysii/ui/basic.dart';
import 'package:zoysii/ui/screens/game/game_page.dart';
import 'package:zoysii/ui/screens/game/widgets/dialogs.dart';
import 'package:zoysii/ui/screens/game/widgets/in_game_snackbar.dart';
import 'package:zoysii/ui/screens/game/widgets/percent_indicator.dart';
import 'package:zoysii/ui/screens/rules/rules_page.dart';
import 'package:zoysii/utils/i18n.dart';
import 'package:zoysii/utils/local_data_controller.dart';
import 'package:zoysii/utils/numeral_system.dart';

class GameAppBar extends StatefulWidget {
  GameAppBar(this.gamePageState, {this.bottom = false});
  final GamePageState gamePageState;
  final bool bottom;

  @override
  GameAppBarState createState() => GameAppBarState();
}

class GameAppBarState extends State<GameAppBar> {
  GameAppBarState() {
    _updateTitle();
  }

  /// Title information displayed
  /// 0: Remaining tiles / Level Name; 1: Moves; 2: Best result / Points away
  int titleType = game!.isMultiMode ? 2 : 0;

  /// App bar title
  late String title;

  /// App bar subtitle
  String? subtitle;

  /// Change title information displayed
  void nextTitle() {
    ++titleType;
    if (titleType > (game!.isLevelMode ? 1 : 2)) titleType = 0;
    _updateTitle();
  }

  /// Update title and subtitle text
  void _updateTitle() {
    switch (titleType) {
      case 0:
        if (game!.isLevelMode) {
          title = game!.level!.name;
          subtitle = null;
        } else {
          title = game!.board!.remainingTiles.toString();
          subtitle = 'remaining tiles'.i18n;
        }
        break;
      case 1:
        title = game!.playerOne.moves.toString();
        subtitle = 'moves'.i18n;
        break;
      case 2:
        title = game!.resultToBeat.toString();
        subtitle = game!.isMultiMode ? 'points away'.i18n : 'best result'.i18n;
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    _updateTitle();
    var children = [
      if (game!.rotatedBox == widget.bottom)
        SizedBox(height: MediaQuery.of(context).padding.top),
      SizedBox(
        height: 70,
        child: ExcludeFocus(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              IconButton(
                icon: const Icon(Icons.close),
                tooltip: 'Exit'.i18n,
                onPressed: () {
                  game!.pause = true;
                  exitDialog(widget.gamePageState);
                },
              ),
              IconButton(
                icon: const Icon(Icons.repeat),
                tooltip: 'Restart'.i18n,
                onPressed: () {
                  game!.pause = true;
                  newMatchDialog(widget.gamePageState);
                },
              ),
              Expanded(
                child: InkWell(
                    borderRadius: const BorderRadius.all(Radius.circular(40)),
                    child: Center(
                      child: Container(
                        padding: const EdgeInsets.symmetric(horizontal: 8),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            FittedBox(
                              fit: BoxFit.fitWidth,
                              child: Text(title,
                                  style: const TextStyle(fontSize: 32)),
                            ),
                            if (subtitle != null)
                              FittedBox(
                                fit: BoxFit.fitWidth,
                                child: Text(
                                  subtitle!,
                                  style: const TextStyle(fontSize: 16),
                                ),
                              ),
                          ],
                        ),
                      ),
                    ),
                    onTap: () {
                      nextTitle();
                      setState(() {});
                    }),
              ),
              NumeralSystemButton(widget.gamePageState),
              IconButton(
                icon: const Icon(Icons.pause),
                tooltip: 'Pause'.i18n,
                onPressed: () {
                  game!.pause = true;
                  pauseDialog(widget.gamePageState.context);
                },
              ),
            ],
          ),
        ),
      ),
      percentIndicator(),
    ];
    return Column(
        children: widget.bottom ? children.reversed.toList() : children);
  }
}

class NumeralSystemButton extends StatelessWidget {
  NumeralSystemButton(this.gamePageState);
  final GamePageState gamePageState;
  final icon = const Icon(Icons.translate);

  @override
  Widget build(BuildContext context) {
    if (!game!.isLevelMode) {
      return nextLevel[0] > 0
          ? PopupMenuButton<NumeralSystem>(
              icon: icon,
              tooltip: 'Numeral system'.i18n,
              shape: menuButtonShape,
              onSelected: (NumeralSystem selectedSystem) {
                game!.selectedNumeralSystem = selectedSystem;
                gamePageState.update();
              },
              itemBuilder: (BuildContext context) {
                var popupEntries = <PopupMenuEntry<NumeralSystem>>[];
                for (final value in NumeralSystem.values) {
                  if (value.index <= nextLevel[0]) {
                    popupEntries.add(PopupMenuItem<NumeralSystem>(
                        value: value, child: Text(value.name.i18n)));
                  }
                }
                return popupEntries;
              },
            )
          : IconButton(
              icon: const Icon(Icons.help_outline),
              tooltip: 'Rules'.i18n,
              onPressed: () {
                game?.pause = true;
                Navigator.push(context, FadeRoute(RulesPage()))
                    .then((_) => game?.pause = false);
              },
            );
    } else {
      return IconButton(
        icon: icon,
        tooltip: 'Numeral system'.i18n,
        onPressed: () {
          game!.selectedNumeralSystem =
              (game!.selectedNumeralSystem != NumeralSystem.Arabic)
                  ? NumeralSystem.Arabic
                  : NumeralSystem.values[game!.level!.levelId[0] + 1];
          gamePageState.update();
          ScaffoldMessenger.of(context).removeCurrentSnackBar();
          ScaffoldMessenger.of(context).showSnackBar(inGameSnackBar(
              context,
              'Numeral system'.i18n +
                  ': ${game!.selectedNumeralSystem.name.i18n}'));
        },
      );
    }
  }
}
