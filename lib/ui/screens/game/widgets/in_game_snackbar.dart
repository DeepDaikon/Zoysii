import 'package:flutter/material.dart';

/// SnackBar used in GamePage
SnackBar inGameSnackBar(BuildContext context, String text, {int seconds = 1}) {
  return SnackBar(
    duration: Duration(seconds: seconds),
    dismissDirection: DismissDirection.vertical,
    content: GestureDetector(
      onTap: ScaffoldMessenger.of(context).hideCurrentSnackBar,
      child: Container(
        constraints: const BoxConstraints(minHeight: 42),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Flexible(child: Text(text, style: const TextStyle(fontSize: 18))),
          ],
        ),
      ),
    ),
  );
}
