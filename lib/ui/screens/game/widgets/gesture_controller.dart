import 'package:flutter/material.dart';
import 'package:zoysii/game/game.dart';
import 'package:zoysii/game/utils/direction.dart';
import 'package:zoysii/ui/screens/game/game_page.dart';

mixin GestureController on State<GamePage> {
  double? _initialX = 0;
  double? _initialY = 0;
  double? _distanceX = 0;
  double? _distanceY = 0;

  /// Widget that detects gestures
  StatelessWidget gestureController(Widget child) {
    return GestureDetector(
        onHorizontalDragStart: (DragStartDetails details) {
          _initialX = details.globalPosition.dx;
        },
        onHorizontalDragUpdate: (DragUpdateDetails details) {
          if (_initialX != null) {
            _distanceX = details.globalPosition.dx - _initialX!;
          }
        },
        onHorizontalDragEnd: (DragEndDetails details) {
          _initialX = 0;
          if (_distanceX != null) {
            game!.inputController(
                _distanceX! > 0 ? Direction.right : Direction.left);
          }
        },
        onVerticalDragStart: (DragStartDetails details) {
          _initialY = details.globalPosition.dy;
        },
        onVerticalDragUpdate: (DragUpdateDetails details) {
          if (_initialY != null) {
            _distanceY = details.globalPosition.dy - _initialY!;
          }
        },
        onVerticalDragEnd: (DragEndDetails details) {
          _initialY = 0;
          if (_distanceY != null) {
            game!.inputController(
                _distanceY! > 0 ? Direction.down : Direction.up);
          }
        },
        child: child);
  }
}
