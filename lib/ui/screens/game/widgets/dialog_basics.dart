import 'package:flutter/material.dart';
import 'package:zoysii/game/game.dart';
import 'package:zoysii/game/utils/direction.dart';
import 'package:zoysii/ui/basic.dart';
import 'package:zoysii/ui/screens/game/game_page.dart';
import 'package:zoysii/ui/screens/game/widgets/in_game_snackbar.dart';
import 'package:zoysii/utils/i18n.dart';

/// In-game alert dialog
class GameDialog extends StatelessWidget {
  GameDialog({
    required this.title,
    required this.content,
    required this.actions,
    this.willPop = true,
    this.titleColor,
  });

  /// Dialog title
  final String title;

  /// Dialog content
  final Widget content;

  /// Dialog buttons
  final List<Widget> actions;

  /// True if dismissible
  final bool willPop;

  /// Title color
  final Color? titleColor;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        game!.pause = !willPop;
        return Future.value(willPop);
      },
      child: AlertDialog(
        elevation: 0,
        shape: const RoundedRectangleBorder(borderRadius: borderRadius),
        title: Container(
          height: 40,
          decoration: BoxDecoration(
            color: titleColor ?? playerColor[1],
            borderRadius: borderRadius,
          ),
          child: Center(
            child: FittedBox(
              fit: BoxFit.fitWidth,
              child: Text(title, style: const TextStyle(color: Colors.white)),
            ),
          ),
        ),
        content: content,
        actions: actions,
      ),
    );
  }
}

/// Player info container used in pause dialog
class PlayerInfo extends StatelessWidget {
  PlayerInfo({
    required this.moves,
    required this.color,
    this.name,
    this.points,
    this.kills = 0,
  });

  /// Player name
  final String? name;

  /// Player points
  final int? points;

  /// PLayer moves
  final int moves;

  /// Player color
  final Color color;

  /// Player kills
  final int kills;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 20),
      padding: const EdgeInsets.only(left: 10, top: 5, bottom: 5),
      decoration: BoxDecoration(
          border: Border(left: BorderSide(color: color, width: 4))),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          if (name != null)
            Container(
                padding: const EdgeInsets.only(bottom: 5),
                child: Text(
                  (name as String) + ' •' * kills,
                  style: TextStyle(
                      color: color, fontSize: 18, fontWeight: FontWeight.bold),
                )),
          if (points != null) Text('Points: %s'.i18n.fill([points!])),
          Text('Moves: %s'.i18n.fill([moves])),
        ],
      ),
    );
  }
}

Widget cancelButton(BuildContext context, {Color? color}) => DialogButton(
      text: 'Cancel'.i18n.toUpperCase(),
      color: color,
      onPressed: () {
        game!.pause = false;
        Navigator.of(context).pop();
      },
    );

Widget restartButton(GamePageState gamePage, {Color? color}) => DialogButton(
      text: 'Restart'.i18n.toUpperCase(),
      color: color,
      onPressed: () {
        game!.start();
        game!.restartCount++;
        gamePage.update();
        Navigator.of(gamePage.context).pop();
      },
    );

void showHint(GamePageState gamePage, List<Direction> moves) {
  game!.start();
  gamePage.update();
  Navigator.of(gamePage.context).pop();
  ScaffoldMessenger.of(gamePage.scaffoldKey.currentContext!)
      .removeCurrentSnackBar();
  ScaffoldMessenger.of(gamePage.scaffoldKey.currentContext!).showSnackBar(
      inGameSnackBar(
          gamePage.context,
          moves.fold('Try this:'.i18n + ' ', (p, e) => p + e.name.i18n + ', ') +
              '…',
          seconds: moves.length));
}

Widget exitButton(BuildContext context, GamePageState gamePage,
    {Color? color}) {
  return DialogButton(
    text: 'Exit'.i18n.toUpperCase(),
    color: color,
    onPressed: () {
      Navigator.pop(context);
      Navigator.pop(gamePage.context);
    },
  );
}

class DialogButton extends StatelessWidget {
  DialogButton({
    required this.text,
    required this.onPressed,
    this.onLongPressed,
    this.color,
  });
  final String text;
  final Color? color;
  final VoidCallback onPressed;
  final VoidCallback? onLongPressed;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      onLongPress: onLongPressed,
      child: Container(
        margin: const EdgeInsets.all(10),
        decoration: color != null
            ? BoxDecoration(
                border: Border(bottom: BorderSide(color: color!, width: 3)))
            : null,
        child: Text(
          text,
          style: TextStyle(
            fontWeight: FontWeight.w600,
            color:
                color == null ? Theme.of(context).colorScheme.secondary : null,
          ),
        ),
      ),
    );
  }
}
