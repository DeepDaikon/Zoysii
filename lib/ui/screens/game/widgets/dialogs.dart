import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:zoysii/game/game.dart';
import 'package:zoysii/ui/basic.dart';
import 'package:zoysii/ui/screens/game/game_page.dart';
import 'package:zoysii/ui/screens/game/widgets/dialog_basics.dart';
import 'package:zoysii/ui/screens/levels/levels_page.dart';
import 'package:zoysii/utils/i18n.dart';

/// Pause match dialog
void pauseDialog(BuildContext context) => showDialog(
      context: context,
      builder: (BuildContext context) {
        return GameDialog(
          title: 'Pause'.i18n,
          content: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text('Remaining tiles: %s'
                    .i18n
                    .fill([game!.board!.remainingTiles])),
                for (var player in game!.ranking)
                  PlayerInfo(
                    name: game!.isMultiMode ? player.name : null,
                    points: !game!.isLevelMode ? player.points : null,
                    moves: player.moves,
                    color: player.displayColor,
                    kills: player.kills,
                  ),
              ],
            ),
          ),
          actions: <Widget>[cancelButton(context)],
        );
      },
    );

/// New / repeat match dialog
void newMatchDialog(GamePageState gamePage) => showDialog(
    context: gamePage.context,
    builder: (BuildContext context) {
      String title;
      Widget content;
      List<Widget> actions;

      if (game!.isLevelMode || game!.loopRepeat) {
        title = 'Restart match'.i18n;
        content = Text('Do you want to restart this match?'.i18n);
        actions = [
          if (game!.restartCount > 3)
            DialogButton(
                text: 'Hint'.i18n.toUpperCase(),
                onPressed: () => showHint(gamePage, game!.level!.hint),
                onLongPressed: () => showHint(gamePage, game!.level!.solution)),
          cancelButton(context),
          restartButton(gamePage, color: playerColor[1]),
        ];
      } else {
        var textFieldController =
            TextEditingController(text: game!.id.toString());
        title = 'New Match'.i18n;
        content = TextFormField(
            textAlign: TextAlign.center,
            controller: textFieldController,
            decoration: InputDecoration(
              helperText:
                  'Insert the Match ID to load.\nLeave it as it is to load a random match.'
                      .i18n,
              helperMaxLines: 8,
            ),
            keyboardType: TextInputType.number,
            inputFormatters: [
              FilteringTextInputFormatter.digitsOnly,
              LengthLimitingTextInputFormatter(7)
            ]);
        actions = [
          cancelButton(context),
          DialogButton(
            text: 'New Match'.i18n.toUpperCase(),
            onPressed: () {
              Navigator.of(context).pop();
              var matchIdInput = int.parse(textFieldController.text.isEmpty
                  ? '-1'
                  : textFieldController.text);
              game!.start(
                  newMatchId: matchIdInput == game!.id ? -1 : matchIdInput);
              gamePage.update();
            },
          ),
          restartButton(gamePage, color: playerColor[1]),
        ];
      }
      return GameDialog(title: title, content: content, actions: actions);
    });

/// Exit match dialog
/// It does not display the dialog if playerOne.moves <= 1
void exitDialog(GamePageState gamePage) {
  if (game!.playerOne.moves <= 1) {
    Navigator.pop(gamePage.context);
  } else {
    showDialog(
      context: gamePage.context,
      builder: (BuildContext context) {
        return GameDialog(
          title: 'Exit'.i18n,
          content: Text(
              'Are you sure you want to quit this game?\nMatch data will be lost.'
                  .i18n),
          actions: <Widget>[
            cancelButton(gamePage.context),
            exitButton(context, gamePage, color: playerColor[1]),
          ],
        );
      },
    );
  }
}

/// End match dialog
void endDialog(GamePageState gamePage,
    {required bool win,
    required String title,
    required String content,
    Color? titleColor}) {
  if (game!.isPaused) return;
  game!.pause = true;
  showDialog(
      context: gamePage.context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return GameDialog(
          willPop: false,
          title: title,
          titleColor: titleColor,
          content: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(content),
                if (!game!.isLevelMode)
                  for (var player in game!.ranking)
                    PlayerInfo(
                      name: game!.isMultiMode ? player.name : null,
                      points: player.points,
                      moves: player.moves,
                      color: player.displayColor,
                      kills: player.kills,
                    ),
                Text('\n' + 'What do you want to do?'.i18n),
              ],
            ),
          ),
          actions: <Widget>[
            exitButton(context, gamePage),
            restartButton(gamePage,
                color: (!game!.isLevelMode || win) ? null : titleColor!),
            if (game!.isLevelMode && game!.level!.levelId[1] == 6 && win)
              DialogButton(
                text: 'Next Layer'.i18n.toUpperCase(),
                color: titleColor!,
                onPressed: () {
                  if (game!.level!.levelId[0] != 0) pageIndex++;
                  pageController.jumpToPage(pageIndex);
                  Navigator.of(context).pop();
                  Navigator.pop(context);
                },
              ),
            if ((!game!.isLevelMode && !game!.loopRepeat) ||
                (game!.isLevelMode && game!.level!.levelId[1] != 6 && win))
              DialogButton(
                color: titleColor ?? playerColor[1],
                text: game!.isLevelMode
                    ? 'Next Level'.i18n.toUpperCase()
                    : 'New Match'.i18n.toUpperCase(),
                onPressed: () {
                  game!.nextMatch();
                  ScaffoldMessenger.of(gamePage.scaffoldKey.currentContext!)
                      .removeCurrentSnackBar();
                  gamePage.update();
                  Navigator.of(context).pop();
                },
              ),
          ],
        );
      });
}
