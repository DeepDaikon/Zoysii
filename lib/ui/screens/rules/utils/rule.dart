/// Game rule
class Rule {
  Rule({
    required this.title,
    required this.subtitle,
    this.image,
    this.lightImage,
    this.border = true,
  });

  /// Rule title
  final String title;

  /// Rule description
  final String subtitle;

  /// Rule image if white theme
  final String? image;

  /// Rule image if dark theme
  final String? lightImage;

  /// True if image should have a border
  final bool border;
}
