import 'package:flutter/material.dart';
import 'package:zoysii/game/utils/player.dart';
import 'package:zoysii/ui/basic.dart';
import 'package:zoysii/utils/i18n.dart';
import 'package:zoysii/utils/local_data_controller.dart';

class MultiPlayerPage extends StatefulWidget {
  @override
  State<MultiPlayerPage> createState() => _MultiPlayerPageState();
}

class _MultiPlayerPageState extends State<MultiPlayerPage> {
  final RangeValues rangeValues = RangeValues(
      settings.minBoardInt.toDouble(), settings.maxBoardInt.toDouble());
  final List<Player> players = [
    for (int i = 0; i < settings.players.length; i++)
      Player(color: playerColor[i], human: settings.players[i]),
  ];
  bool get oneHuman => players.where((p) => p.human).length == 1;
  final List<String> speedStrings = [
    'Turn-based'.i18n,
    'Low'.i18n,
    'Medium'.i18n,
    'High'.i18n,
    'Very high'.i18n,
  ];
  final List<int> speedValues = [0, 1500, 1200, 900, 600];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text('Multiplayer'.i18n.toUpperCase()), centerTitle: true),
      body: ListView(
        padding: const EdgeInsets.fromLTRB(8, 8, 8, 70),
        children: [slider(), checkbox(), const Divider(), ...playerTileList()],
      ),
      floatingActionButton: PlayButton(
        players: players,
        gameSpeed: oneHuman ? speedValues[settings.speedMulti] : 0,
        callback: () {
          settings.players = players.fold([], (l, e) => [...l, e.human]);
          saveSettings();
          for (final player in players) {
            player.clear();
          }
        },
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }

  Widget slider() {
    return Theme(
      data: Theme.of(context).copyWith(dividerColor: Colors.transparent),
      child: IgnorePointer(
        ignoring: !oneHuman,
        child: ExpansionTile(
          title: Text('Speed'.i18n, style: const TextStyle(fontSize: 18)),
          subtitle: Text(
            'Game speed'.i18n,
            style: TextStyle(color: Theme.of(context).textTheme.caption!.color),
          ),
          trailing: Text(
            oneHuman ? speedStrings[settings.speedMulti] : speedStrings[0],
            style: const TextStyle(fontSize: 16),
          ),
          initiallyExpanded: false,
          children: <Widget>[
            if (oneHuman)
              Slider(
                value: settings.speedMulti.toDouble(),
                min: 0,
                max: 4,
                onChanged: (double newValue) {
                  setState(() => settings.speedMulti = newValue.round());
                  saveSettings();
                },
              ),
          ],
        ),
      ),
    );
  }

  Widget checkbox() => CheckboxListTile(
      title: Text('Rotate screen'.i18n, style: const TextStyle(fontSize: 18)),
      subtitle: Text(
          "Rotate the screen when it is a friend's turn. This is useful if you are facing each other."
              .i18n),
      value: settings.rotatedDisplay,
      onChanged: oneHuman
          ? null
          : (newValue) => setState(() {
                settings.rotatedDisplay = newValue!;
                saveSettings();
              }));

  List<Widget> playerTileList() => [
        for (final player in players)
          Column(
            children: [
              ListTile(
                onTap: () => setState(() => player.human =
                    !player.human || players.where((p) => p.human).length < 2),
                leading: CircleAvatar(
                  backgroundColor: player.color,
                  child: Icon(
                    player.human ? Icons.person : Icons.android,
                    color: Colors.white,
                  ),
                ),
                title: Text(player.name, style: const TextStyle(fontSize: 18)),
                trailing: players.length > 2 &&
                        (!player.human ||
                            players.where((p) => p.human).length > 1)
                    ? IconButton(
                        icon: const Icon(Icons.clear),
                        tooltip: 'Remove'.i18n,
                        onPressed: () {
                          setState(() {
                            players.remove(player);
                          });
                        },
                      )
                    : null,
              ),
              const Divider(color: Colors.transparent),
            ],
          ),
        if (players.length < 4) ...[
          const Divider(),
          TextButton(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [const Icon(Icons.add), Text('Add new player'.i18n)],
            ),
            onPressed: () => setState(() {
              players.add(Player(
                  color: playerColor.firstWhere(
                      (c) => players.every((p) => p.displayColor != c)),
                  human: true));
            }),
          ),
          const Divider(),
        ],
      ];
}
