import 'package:flutter/material.dart';
import 'package:zoysii/ui/basic.dart';
import 'package:zoysii/ui/screens/ranking/ranking_page.dart';
import 'package:zoysii/utils/i18n.dart';

/// Bottom bar used to filter and sort results
class BottomFilterBar extends StatelessWidget {
  BottomFilterBar(this.rankingPageState);

  /// State of the ranking page
  final RankingPageState rankingPageState;

  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
      child: Container(
        color: const Color(0xFF244A63),
        height: kToolbarHeight,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [filterButton(), sortButton()],
        ),
      ),
    );
  }

  /// Widget used to filter results
  Widget filterButton() {
    var popupEntries = <PopupMenuEntry<int>>[];
    for (final i in [5, 6, 7, 8, 9, 10, 0]) {
      if (matchResults.where((result) => result.sideLength == i).isNotEmpty ||
          (i == 0 && popupEntries.length > 1)) {
        popupEntries.add(
          PopupMenuItem<int>(
            value: i,
            child: Text(
              i != 0 ? '${i}x$i' : 'Show all'.i18n,
              style: const TextStyle(fontSize: 20),
            ),
          ),
        );
      }
    }
    if (popupEntries.length < 2) popupEntries.clear();
    return Expanded(
      child: PopupMenuButton<int>(
        shape: menuButtonShape,
        tooltip: 'Filter'.i18n,
        onSelected: (int filterChoice) {
          filter = filterChoice;
          rankingPageState.refresh();
        },
        itemBuilder: (BuildContext context) => popupEntries,
        child: SizedBox(
          height: kToolbarHeight,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: const Icon(Icons.filter_list, color: Colors.white)),
              Flexible(
                child: Text(
                  filter != 0 ? '$filter x $filter' : 'All'.i18n,
                  style: const TextStyle(fontSize: 20, color: Colors.white),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  /// Widget used to sort results
  Widget sortButton() => Expanded(
        child: PopupMenuButton<SortOptions>(
          shape: menuButtonShape,
          tooltip: 'Sort'.i18n,
          onSelected: (SortOptions sortChoice) {
            sorter = sortChoice;
            rankingPageState.refresh();
          },
          itemBuilder: (BuildContext context) {
            var popupEntries = <PopupMenuEntry<SortOptions>>[];
            for (final option in SortOptions.values) {
              popupEntries.add(
                PopupMenuItem<SortOptions>(
                  value: option,
                  child: Text(
                    option.name.i18n,
                    style: const TextStyle(fontSize: 20),
                  ),
                ),
              );
            }
            return popupEntries;
          },
          child: SizedBox(
            height: kToolbarHeight,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Flexible(
                  child: Text(
                    sorter.name.i18n,
                    style: const TextStyle(fontSize: 20, color: Colors.white),
                  ),
                ),
                Container(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: const Icon(Icons.sort, color: Colors.white),
                )
              ],
            ),
          ),
        ),
      );
}
