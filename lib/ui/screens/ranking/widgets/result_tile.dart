import 'package:flutter/material.dart';
import 'package:zoysii/game/game.dart';
import 'package:zoysii/game/utils/board.dart';
import 'package:zoysii/game/utils/random.dart';
import 'package:zoysii/game/utils/result.dart';
import 'package:zoysii/ui/basic.dart';
import 'package:zoysii/ui/screens/game/game_page.dart';
import 'package:zoysii/ui/screens/ranking/ranking_page.dart';
import 'package:zoysii/utils/i18n.dart';

/// Tile that contains result data
class ResultTile extends StatelessWidget {
  ResultTile(
    this.index,
    this.result, {
    required this.showBigLow,
    required this.showRange,
    this.callback,
  });

  /// Result index
  final int index;

  /// Result displayed
  final Result result;

  /// True if biggest low must be shown
  final bool showBigLow;

  /// True if range of tile must be shown
  final bool showRange;

  /// Callback
  final VoidCallback? callback;

  @override
  Widget build(BuildContext context) {
    // Start date as a DateTime
    var startDate = DateTime.fromMillisecondsSinceEpoch(result.startDate);

    // Start date string formatted as "yyyy-MM-dd"
    String startDateString;
    var now = DateTime.now();
    if (startDate.day == now.day &&
        startDate.month == now.month &&
        startDate.year == now.year) {
      startDateString = 'Today'.i18n;
    } else {
      startDateString = startDate.toString().split(' ')[0];
    }

    // Tile details
    String? title;
    String trailing;
    String subtitle;

    switch (sorter) {
      case SortOptions.Moves:
        trailing = result.moves.toString();
        break;
      case SortOptions.Points:
        trailing = result.points.toString();
        break;
      case SortOptions.Date:
        trailing = startDateString;
        title = '%s points'.i18n.fill([result.points]);
        break;
    }

    subtitle = filter > 0
        ? 'in %s min'.i18n.fill([result.formattedDuration])
        : '${result.sideLength}x${result.sideLength}';

    // Return a list of DetailTile
    // This is the content displayed when ResultTile is expanded
    List<Widget> resultDetailsWidgetList() => [
          if (sorter == SortOptions.Moves)
            DetailTile('Points'.i18n, result.points.toString())
          else
            DetailTile('Moves'.i18n, result.moves.toString()),
          DetailTile('Size'.i18n, result.sideLength.toString()),
          if (showRange)
            DetailTile('Range of values'.i18n,
                '${result.minBoardInt} - ${result.maxBoardInt}'),
          if (showBigLow)
            DetailTile('Biggest low'.i18n, result.biggestLow.toString()),
          DetailTile(
              'Date'.i18n,
              sorter != SortOptions.Date
                  ? startDateString
                  : startDateString = startDate.toString().split('.')[0]),
          if (filter == 0)
            DetailTile('Duration'.i18n, result.formattedDuration),
          DetailTile('Match ID'.i18n, result.matchId.toString()),
        ];

    return ExpansionTile(
      leading: CircleAvatar(
        backgroundColor: Colors.blueGrey[500],
        child: Text(
          '${index + 1}',
          style: const TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
      title: Text(
        title ?? startDateString,
        style: const TextStyle(fontSize: 20),
      ),
      subtitle: Text(
        subtitle,
        style: TextStyle(
            fontSize: 16, color: Theme.of(context).textTheme.caption!.color),
      ),
      trailing: Text(trailing, style: const TextStyle(fontSize: 20)),
      children: [
        ...resultDetailsWidgetList(),
        TextButton(
          child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [Text('Retry this match'.i18n)]),
          onPressed: () {
            CurrentRandom.set(result.matchId);
            Navigator.push(
              context,
              FadeRoute(GamePage(
                GameMode.single,
                matchToLoad: result.matchId,
                board: Board(
                    width: result.sideLength,
                    height: result.sideLength,
                    minValue: result.minBoardInt,
                    maxValue: result.maxBoardInt,
                    biggestLow: result.biggestLow),
              )),
            ).then((_) => callback?.call());
          },
        ),
      ],
    );
  }
}

/// ListTile that contains a single match detail
class DetailTile extends StatelessWidget {
  DetailTile(this.title, this.value);
  final String title;
  final String value;

  @override
  Widget build(BuildContext context) => Container(
        color: Colors
            .grey[Theme.of(context).brightness == Brightness.dark ? 800 : 300],
        child: ListTile(title: Text(title), trailing: Text(value)),
      );
}
