import 'package:flutter/material.dart';
import 'package:zoysii/ui/screens/settings/settings_page.dart';
import 'package:zoysii/utils/i18n.dart';
import 'package:zoysii/utils/local_data_controller.dart';
import 'package:zoysii/utils/numeral_system.dart';

/// Dialog that shows a radio list to select numeral system
Future numeralSystemDialog(SettingsPageState settingsPageState) {
  return showDialog(
    context: settingsPageState.context,
    builder: (BuildContext context) => ScaffoldMessenger(
      child: Builder(
        builder: (context) => Scaffold(
          backgroundColor: Colors.transparent,
          body: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: () => Navigator.of(context).pop(),
            child: AlertDialog(
              title: Text('Select a Numeral System'.i18n),
              contentPadding: const EdgeInsets.symmetric(vertical: 20),
              content: SingleChildScrollView(
                child: Column(
                  children: List.generate(NumeralSystem.values.length, (index) {
                    return RadioListTile<int>(
                      title: Text(
                        NumeralSystem.values[index].name.i18n +
                            (index > nextLevel[0] ? ' ' + '(locked)'.i18n : ''),
                      ),
                      value: index,
                      groupValue:
                          NumeralSystem.values.indexOf(settings.numeralSystem),
                      onChanged: (int? value) {
                        ScaffoldMessenger.of(context).hideCurrentSnackBar();
                        if (index > nextLevel[0]) {
                          ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(
                              duration: const Duration(seconds: 1),
                              content: Text(
                                'You have to complete Layer %s in levels mode to unlock this feature'
                                    .i18n
                                    .fill(
                                  [index],
                                ),
                              ),
                            ),
                          );
                        } else {
                          settings.numeralSystem = NumeralSystem.values[value!];
                          settingsPageState.refresh();
                          saveSettings();
                          Navigator.of(context).pop();
                        }
                      },
                    );
                  }),
                ),
              ),
            ),
          ),
        ),
      ),
    ),
  );
}
