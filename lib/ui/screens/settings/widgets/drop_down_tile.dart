import 'package:flutter/material.dart';
import 'package:zoysii/ui/screens/settings/settings_page.dart';
import 'package:zoysii/utils/local_data_controller.dart';

/// Tile widget for options that use drop down button
class DropDownTile extends StatelessWidget {
  DropDownTile(this.option, this.title, this.subtitle, this.value, this.options,
      {required this.settingsPageState, this.mapText});

  /// Variable name to manage
  final String option;

  /// Tile title
  final String title;

  /// Tile subtitle
  final String subtitle;

  /// Drop down value
  final int value;

  /// Possible options
  final List<int> options;

  /// Map value-text
  final Map<int, String>? mapText;

  /// State of the settings page
  final SettingsPageState settingsPageState;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(title, style: const TextStyle(fontSize: 20)),
      subtitle: Text(subtitle),
      trailing: DropdownButton<int>(
        value: value,
        onChanged: (int? newValue) {
          _updateVariables(option, newValue!);
          saveSettings();
          settingsPageState.refresh();
        },
        items: options
            .map<DropdownMenuItem<int>>(
              (int value) => DropdownMenuItem<int>(
                value: value,
                child: Text(
                  (mapText != null ? mapText![value] : value.toString())!
                      .padRight(2, '  '),
                ),
              ),
            )
            .toList(),
      ),
    );
  }

  /// Update the right variable for each option
  void _updateVariables(String option, int newValue) {
    switch (option) {
      case 'sideLength':
        settings.sideLength = newValue;
        break;
      case 'biggestLow':
        settings.biggestLow = newValue;
        break;
      case 'inputMethod':
        settings.inputMethod = newValue;
        break;
      case 'gamepadShape':
        settings.gamepadShape = newValue;
        settings.gamepadOffset = Offset.zero;
        break;
      case 'gamepadSize':
        settings.gamepadSize = newValue.toDouble();
        settings.gamepadOffset = Offset.zero;
        break;
      case 'theme':
        settings.theme = ThemeMode.values[newValue];
        settingsPageState.fullRefresh();
        break;
    }
  }
}
