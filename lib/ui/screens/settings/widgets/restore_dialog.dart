import 'package:flutter/material.dart';
import 'package:i18n_extension/i18n_widget.dart';
import 'package:zoysii/ui/screens/settings/settings_page.dart';
import 'package:zoysii/utils/i18n.dart';
import 'package:zoysii/utils/local_data_controller.dart';
import 'package:zoysii/utils/settings.dart';

/// Dialog used to restore default settings
Future restoreSettingsDialog(SettingsPageState settingsPageState) {
  return showDialog(
    context: settingsPageState.context,
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text('Restore default settings?'.i18n),
        content: Text(
            'Are you sure you want to delete your settings and restore default ones?'
                .i18n),
        actions: <Widget>[
          TextButton(
            child: Text('Restore'.i18n,
                style:
                    TextStyle(color: Theme.of(context).colorScheme.secondary)),
            onPressed: () {
              settings = Settings({'firstRun': false});
              I18n.of(context).locale = settings.locale;
              saveSettings();
              settingsPageState.fullRefresh();
              Navigator.of(context).pop();
            },
          ),
          TextButton(
            onPressed: Navigator.of(context).pop,
            child: Text('Cancel'.i18n,
                style:
                    TextStyle(color: Theme.of(context).colorScheme.secondary)),
          ),
        ],
      );
    },
  );
}
