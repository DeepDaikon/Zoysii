import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:zoysii/ui/screens/info/widgets/license_dialog.dart';
import 'package:zoysii/utils/i18n.dart';
import 'package:zoysii/utils/local_data_controller.dart';

class InfoPage extends StatefulWidget {
  @override
  State<InfoPage> createState() => _InfoPageState();
}

class _InfoPageState extends State<InfoPage> {
  /// List of menu items displayed in the info page
  final infoMenuList = <Map<String, dynamic>>[
    {
      'title': 'DeepDaikon Project',
      'subtitle': 'App developed by %s'.i18n.fill(['DeepDaikon']),
      'url': 'https://deepdaikon.xyz',
      'icon': const Icon(Icons.change_history),
    },
    {
      'title': 'Version: %s'.i18n.fill(['2.4.3']),
      'subtitle': 'App version'.i18n,
      'icon': const Icon(Icons.looks_two),
    },
    {
      'title': 'Donate'.i18n,
      'subtitle': 'Support the development'.i18n,
      'url': 'https://deepdaikon.xyz/donate',
      'icon': const Icon(Icons.euro),
    },
    {
      'title': 'Translate'.i18n,
      'subtitle': 'Translate in your language'.i18n,
      'url': 'https://translate.deepdaikon.xyz/engage/zoysii/',
      'icon': const Icon(Icons.translate_rounded),
    },
    {
      'title': 'Send email'.i18n,
      'subtitle': 'Ask for something or request a new feature'.i18n,
      'url': 'mailto:deepdaikon' '@' 'tuta.io?subject=Zoysii Game',
      'icon': const Icon(Icons.email),
    },
    {
      'title': 'Report bugs'.i18n,
      'subtitle': 'Report bugs or request new feature'.i18n,
      'url': 'https://gitlab.com/deepdaikon/Zoysii/issues',
      'icon': const Icon(Icons.bug_report_rounded),
    },
    {
      'title': 'View source code'.i18n,
      'subtitle': 'Look at the source code'.i18n,
      'url': 'https://gitlab.com/deepdaikon/Zoysii',
      'icon': const Icon(Icons.developer_mode),
    },
    {
      'title': 'View License'.i18n,
      'subtitle': 'Read software license'.i18n,
      'url': 'https://gitlab.com/deepdaikon/Zoysii/blob/master/LICENSE',
      'icon': const Icon(Icons.chrome_reader_mode),
    },
    {
      'title': 'Third Party Licenses'.i18n,
      'subtitle': 'Read third party notices'.i18n,
      'icon': const Icon(Icons.code),
    }
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Info'.i18n.toUpperCase()), centerTitle: true),
      body: ListView.builder(
        padding: const EdgeInsets.all(8),
        itemCount: infoMenuList.length,
        itemBuilder: (BuildContext context, int index) {
          return ListTile(
            leading: Icon(infoMenuList[index]['icon'].icon, size: 27),
            title: Text(
              infoMenuList[index]['title'],
              style: const TextStyle(fontSize: 20),
            ),
            subtitle: Text(infoMenuList[index]['subtitle']),
            onTap: () {
              if (infoMenuList[index].containsKey('url')) {
                launchUrl(Uri.parse(infoMenuList[index]['url']),
                    mode: LaunchMode.externalApplication);
              } else if (infoMenuList[index]['title'] ==
                  'Third Party Licenses'.i18n) {
                licenseDialog(context);
              } else if (++_debugClickCount == 17) {
                ScaffoldMessenger.of(context).showSnackBar(
                  SnackBar(
                    duration: const Duration(milliseconds: 2500),
                    content: Text(
                      'Warning: debug mode enabled'.i18n,
                      textAlign: TextAlign.center,
                    ),
                  ),
                );
                nextLevel = [layers.length, 0];
              }
            },
          );
        },
      ),
    );
  }

  /// Counter used to enter in debug mode
  int _debugClickCount = 0;
}
