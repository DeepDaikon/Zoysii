import 'dart:math';

// ignore: avoid_classes_with_only_static_members
class CurrentRandom {
  static late Random _currentRandom;

  static int set(int? inputSeed) {
    var currentSeed =
        (inputSeed ?? -1) == -1 ? Random().nextInt(10e6.toInt()) : inputSeed!;
    _currentRandom = Random(currentSeed);
    return currentSeed;
  }

  static int get(int min, int max) =>
      min + _currentRandom.nextInt(max - min + 1);
}
