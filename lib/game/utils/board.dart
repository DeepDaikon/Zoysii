import 'dart:math';

import 'package:zoysii/game/utils/level.dart';
import 'package:zoysii/game/utils/random.dart';
import 'package:zoysii/utils/local_data_controller.dart';

class Board {
  Board({
    this.minValue,
    this.maxValue,
    this.width = 0,
    this.height = 0,
    this.biggestLow,
  }) {
    minValue ??= settings.minBoardInt;
    maxValue ??= settings.maxBoardInt;
    biggestLow ??= settings.biggestLow;
    if (width == 0) width = settings.sideLength;
    if (height == 0) height = width;
    grid = List.generate(
        width * height, (index) => CurrentRandom.get(minValue!, maxValue!));
  }

  Board.fromLevel(Level level)
      : _deleteAll = true,
        grid = List.from(level.grid),
        width = level.width,
        height = level.grid.length ~/ level.width;

  /// List of tiles
  late List<int?> grid;

  /// Board settings
  int width;
  int height;
  int? minValue;
  int? maxValue;
  int? biggestLow;

  /// True if every tile should be deleted (eg level mode)
  bool _deleteAll = false;

  /// Number of zeros in the board
  int get _zeros => grid.where((tile) => tile == 0 || tile == null).length;

  /// Tiles still to be deleted
  int get remainingTiles => _deleteAll
      ? grid.length - _zeros
      : max(width * (height - 1) - _zeros - 1, 0);
}
